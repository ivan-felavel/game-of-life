#include <iostream>
#include <vector>
#include <string>
using namespace std;


int f, c;

bool verifica(int i, int j)
{
	if (i < 0 || i >= f || j < 0 || j >= c)
		return false;
	return true;
}

int main(int argc, char const *argv[])
{

	freopen("t.input", "r", stdin);
	freopen("t.out", "w", stdout);
	
	int cases;
	cin >> cases;
	while(cases--)
	{
		cin >> f >> c;
		vector <vector <int>> mat;
		vector <vector <int>> n_mat;
		vector <int> auxiliar;
		string auxi;
		for (int i = 0; i < f; ++i)
		{
			cin >> auxi;
			auxiliar.clear();
			for (int j = 0; j < c; ++j)
			{
				if (auxi[j] == '*')
					auxiliar.push_back(1);
				else 
					auxiliar.push_back(0);
			}

			mat.push_back(auxiliar);
		}
		int times;
		cin >> times;
		n_mat = mat;
		while(times--)
		{
			int vecinos = 0;
			for (int i = 0; i < mat.size(); ++i)
			{
				for (int j = 0; j < mat[i].size(); ++j)
				{
					vecinos = 0;
					n_mat[i][j] = 0;

					verifica(i,j+1) ? mat[i][j+1] == 1 ? vecinos++ : false : false;
					verifica(i+1,j+1) ? mat[i+1][j+1] == 1 ? vecinos++ : false : false;
					verifica(i+1,j) ? mat[i+1][j] == 1 ? vecinos++ : false : false;
					verifica(i+1,j-1) ? mat[i+1][j-1] == 1 ? vecinos++ : false : false;
					verifica(i,j-1) ? mat[i][j-1] == 1 ? vecinos++ : false : false;
					verifica(i-1,j-1) ? mat[i-1][j-1] == 1 ? vecinos++ : false : false;
					verifica(i-1,j) ? mat[i-1][j] == 1 ? vecinos++ : false : false;
					verifica(i-1,j+1) ? mat[i-1][j+1] == 1 ? vecinos++ : false : false;

//					cout << vecinos << " ";

					if (mat[i][j] == 1)
					{
						if (vecinos < 2 || vecinos > 3)
							n_mat[i][j] = 0;
						else if (vecinos == 3 || vecinos == 2)
							n_mat[i][j] = 1;
					}

					if (mat[i][j] == 0)
						if (vecinos == 3)
							n_mat[i][j] = 1;


				}
				//cout << endl;
			}
			mat = n_mat;
			
		}

		for (int i = 0; i < mat.size(); ++i)
		{
			for (int j = 0; j < mat[i].size(); ++j)
			{
				if (mat[i][j] == 0)
					cout << ".";
				else
					cout << "*";
			}
			cout << endl;
		}

	}
	return 0;
}