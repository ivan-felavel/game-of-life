var w;
var cols = 15;
var rows = 15;
var mat = [];
var n_mat = [];


for (var i = 0; i < rows; i++) {
    mat.push([]);
    n_mat.push([])
    for (var j = 0; j < cols; j++) {
        mat[i].push(0);
        n_mat[i].push(0);
    }
}

console.log(mat);


function verifica(i, j) {
    if (i >= 0 && i < rows && j >= 0 && j < cols) {
        return true;
    } else {
        return false;
    }
}

function juego() {
    var k = 10;
    console.log("juego...")
    console.log(mat);
    for (var i = 0; i < rows; ++i) {
        for (var j = 0; j < cols; ++j) {
            vecinos = 0;
            n_mat[i][j] = 0;

            verifica(i, j + 1) ? mat[i][j + 1] == 1 ? vecinos++ : false : false;
            verifica(i + 1, j + 1) ? mat[i + 1][j + 1] == 1 ? vecinos++ : false : false;
            verifica(i + 1, j) ? mat[i + 1][j] == 1 ? vecinos++ : false : false;
            verifica(i + 1, j - 1) ? mat[i + 1][j - 1] == 1 ? vecinos++ : false : false;
            verifica(i, j - 1) ? mat[i][j - 1] == 1 ? vecinos++ : false : false;
            verifica(i - 1, j - 1) ? mat[i - 1][j - 1] == 1 ? vecinos++ : false : false;
            verifica(i - 1, j) ? mat[i - 1][j] == 1 ? vecinos++ : false : false;
            verifica(i - 1, j + 1) ? mat[i - 1][j + 1] == 1 ? vecinos++ : false : false;

            if (mat[i][j] == 1) {
                if (vecinos < 2 || vecinos > 3)
                    n_mat[i][j] = 0;
                else if (vecinos == 3 || vecinos == 2)
                    n_mat[i][j] = 1;
            }

            if (mat[i][j] == 0)
                if (vecinos == 3)
                    n_mat[i][j] = 1;


        }
    }
    for (var i = 0; i < rows; i++) {
        for (var j = 0; j < cols; j++) {
            mat[i][j] = n_mat[i][j];
        }
    }


    var respuesta = [];

    for (var i = 0; i < rows; i++) {
        for (var j = 0; j < cols; j++) {
            respuesta.push(mat[i][j]);
        }

    }


    var x = document.getElementById("tablero").getElementsByTagName("td");
    for (var i = 0; i < respuesta.length; i++) {
        if (respuesta[i] == 1)
            x[i].style.backgroundColor = "black";
        else
            x[i].style.backgroundColor = "white";
    }

}

var grid = clickableGrid(rows, cols, function (el, row, col, i) {
    el.className = 'clicked';
    mat[row][col] = 1;
    console.log("You clicked on element:", mat[row][col]);
    console.log("You clicked on row:", row);
    console.log("You clicked on col:", col);

});

document.body.appendChild(grid);

function clickableGrid(rows, cols, callback, dobleclick) {
    var i = 0;
    var grid = document.createElement('table');
    grid.setAttribute("id", "tablero");
    grid.className = 'grid';
    for (var r = 0; r < rows; ++r) {
        var tr = grid.appendChild(document.createElement('tr'));
        for (var c = 0; c < cols; ++c) {
            var cell = tr.appendChild(document.createElement('td'));
            //cell.setAttribute("id", "celda");
            //cell.style.backgroundColor = "white";
            cell.addEventListener('click', (function (el, r, c, i) {
                return function () {
                    callback(el, r, c, i);
                }
            })(cell, r, c, i), false);

        }
    }

    return grid;
}