

rows = 25
cols = 25

#matriz de estados y la matriz auxiliar 
grid = [ [-1]*rows  for n in range(cols)] 
grid_auxi = [ [-1]*rows  for n in range(cols)] 

#tamanio de las celadas
w = 20 

	
#se crea el grid 
def setup():
	size(800,600)
	
#dibuja la cuadri
def draw():
	global grid
	x = 0
	y = 0
	for row in grid:
		for col in row:
			if col == 1:
				fill(0,0,0)
			else:
				fill(255)
			rect(x, y, w, w)
			x = x + w 
		y = y + w 
		x = 0 
		
#para que al dar clic cambie de estado la celda        
def mousePressed():
	grid[mouseY/w][mouseX/w] = -1 * grid[mouseY/w][mouseX/w]  


#al dar ENTER pasamos al siguiente estado del grid
def keyPressed():
	if key == ENTER:
		juego()
		
#el juego de la vida
def juego():
	global grid, grid_auxi
	global rows, cols
	print "antes"
	for x in grid:
		print x
	for i  in range(1,  rows - 1):
		for j in range(1, cols - 1):
			grid_auxi[i][j] = -1
			vecinos = 0

			if grid[i-1][j-1] == 1:
				vecinos += 1
			if grid[i-1][j] == 1:
				vecinos += 1
			if grid[i-1][j+1] == 1:
				vecinos += 1
			if grid[i][j-1] == 1:
				vecinos += 1
			if grid[i][j+1] == 1:
				vecinos += 1
			if grid[i+1][j-1] == 1:
				vecinos += 1
			if grid[i+1][j] == 1:
				vecinos += 1
			if grid[i+1][j+1] == 1:
				vecinos += 1
				
			if grid[i][j] == 1:                
				if vecinos < 2 or vecinos > 3:
					 grid_auxi[i][j] = -1
				if vecinos == 2 or vecinos == 3:
					 grid_auxi[i][j] = 1

			if grid[i][j] == -1:
			   if vecinos == 3:
					 grid_auxi[i][j] = 1
	

	for i in range(0, len(grid)):
		for j in range(0, len(grid[i])):
			grid[i][j] = grid_auxi[i][j]
